from django.utils.translation import ugettext_lazy as _
from django.apps import AppConfig


class MartyrsConfig(AppConfig):
    name = 'martyrs'
    verbose_name = _("#Jan25 Martyrs")
