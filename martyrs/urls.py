from django.conf.urls import url
from martyrs import views


urlpatterns = [
    url(r'^new/$', views.MartyrsCreateView.as_view(), name='create'),
    url(r'^$', views.MartyrsListView.as_view(), name='list'),
    url(r'^(?P<pk>\d+)/$', views.MartyrsDetailView.as_view(), name='detail'),
    url(r'^(?P<pk>\d+)/update/$', views.MartyrsUpdateView.as_view(), name='update'),
    url(r'^(?P<pk>\d+)/delete/$', views.MartyrsDeleteView.as_view(), name='delete'),
]
