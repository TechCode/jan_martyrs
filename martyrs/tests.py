from datetime import date
from django.test import TestCase
from django.contrib.auth import get_user_model
from django.core.urlresolvers import reverse
from martyrs.models import Martyr

User = get_user_model()

class TestMartyrModel(TestCase):
    def test_create(self):
        '''Dumy test for model, so basic config of project is working!'''
        martyr = Martyr.objects.create(
            name='name',
            birth_date=date(day=1, month=1, year=2000),
            death_date=date(day=28, month=1, year=2010),
            death_cause='direct shot',
            officer_name='officer',
            governorate='Cairo',
        )
        self.assertEqual(Martyr.objects.count(), 1)

        self.assertEqual(str(martyr), 'Martyr: name')


class TestMartyrsViews(TestCase):
    @classmethod
    def setUpTestData(cls):
        User.objects.create_user(
            username='username1',
            email='email@example.com',
            password='password'
        )
        martyrs = [
            Martyr(
                name='name %d' % i,
                birth_date=date(day=1, month=1, year=2000),
                death_date=date(day=28, month=1, year=2010),
                death_cause='direct shot',
                officer_name='officer',
                governorate='Cairo',
            )
            for i in range(10)
        ]
        Martyr.objects.bulk_create(martyrs)

    def setUp(self):
        self.client.login(username='username1', password='password')

    def test_list(self):
        response = self.client.get(reverse('martyrs:list'))
        self.assertEqual(response.status_code, 200)
        self.assertContains(response, 'name 1')
        self.assertContains(response, 'officer')
        self.assertTemplateUsed(response, 'martyrs/list.html')

    def test_detail(self):
        response = self.client.get(reverse('martyrs:detail', kwargs={'pk': 1}))
        self.assertEqual(response.status_code, 200)
        self.assertContains(response, 'name')
        self.assertContains(response, 'officer')
        self.assertTemplateUsed(response, 'martyrs/detail.html')

    def test_create(self):
        response = self.client.get(reverse('martyrs:create'))
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'martyrs/create.html')
        self.assertTemplateUsed(response, 'martyrs/form.html')

        response = self.client.post(
            reverse('martyrs:create'),
            data={
                'name': 'name 2',
                'birth_date': date(day=10, month=1, year=2000),
                'death_date': date(day=28, month=1, year=2010),
                'death_cause': 'direct shot',
                'officer_name': 'officer',
                'governorate': 'Cairo',
            }
        )
        self.assertEqual(response.status_code, 302)

    def test_update(self):
        response = self.client.get(reverse('martyrs:update', kwargs={'pk': 1}))
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'martyrs/update.html')
        self.assertTemplateUsed(response, 'martyrs/form.html')

        response = self.client.post(
            reverse('martyrs:update', kwargs={'pk': 1}),
            data={
                'name': 'Updated name',
                'birth_date': date(day=1, month=1, year=2000),
                'death_date': date(day=28, month=1, year=2010),
                'death_cause': 'direct shot',
                'officer_name': 'officer',
                'governorate': 'Cairo',
            }
        )
        self.assertEqual(response.status_code, 302)

    def test_delete(self):
        response = self.client.get(reverse('martyrs:delete', kwargs={'pk': 1}))
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'martyrs/delete.html')

        response = self.client.delete(reverse('martyrs:delete', kwargs={'pk': 1}))
        self.assertEqual(response.status_code, 302)

    def test_login_required(self):
        self.client.logout()
        actions = [
            reverse('martyrs:create'),
            reverse('martyrs:update', kwargs={'pk': 1}),
            reverse('martyrs:delete', kwargs={'pk': 1}),
        ]
        for action in actions:
            self.assertRedirects(
                self.client.get(action),
                reverse('login') + "?next=%s" % action
            )

        safe_actions = [
            reverse('martyrs:list'),
            reverse('martyrs:detail', kwargs={'pk': 1})
        ]
        for action in safe_actions:
            self.assertEqual(self.client.get(action).status_code, 200)
