from django import forms
from martyrs.models import Martyr


class MartyrForm(forms.ModelForm):
    class Meta:
        model = Martyr
        fields = ('name', 'birth_date', 'death_date', 'death_cause', 'officer_name', 'governorate',)
