from django.views.generic import (ListView, DetailView, CreateView, UpdateView,
                                  DeleteView)
from django.contrib.auth.decorators import login_required
from django.utils.decorators import method_decorator
from django.core.urlresolvers import reverse
from martyrs.models import Martyr
from martyrs.forms import MartyrForm


class LoginRequiredMixin(object):
    @method_decorator(login_required)
    def dispatch(self, request, *args, **kwargs):
        return super(LoginRequiredMixin, self).dispatch(request, *args, **kwargs)


class MartyrsCreateView(LoginRequiredMixin, CreateView):
    model = Martyr
    template_name = 'martyrs/create.html'
    form_class = MartyrForm

    def get_success_url(self):
        """redirect to martyr page on success"""
        return reverse('martyrs:detail', kwargs={'pk': self.object.pk})


class MartyrsListView(ListView):
    model = Martyr
    template_name = 'martyrs/list.html'


class MartyrsDetailView(DetailView):
    model = Martyr
    template_name = 'martyrs/detail.html'


class MartyrsUpdateView(LoginRequiredMixin, UpdateView):
    model = Martyr
    template_name = 'martyrs/update.html'
    form_class = MartyrForm

    def get_success_url(self):
        """redirect to martyr page on success"""
        return reverse('martyrs:detail', kwargs={'pk': self.object.pk})


class MartyrsDeleteView(LoginRequiredMixin, DeleteView):
    model = Martyr
    template_name = 'martyrs/delete.html'

    def get_success_url(self):
        """redirect to martyr list on success"""
        return reverse('martyrs:list')
