from django.utils.translation import ugettext_lazy as _
from django.utils.encoding import python_2_unicode_compatible
from django.db import models


@python_2_unicode_compatible
class Martyr(models.Model):
    name = models.CharField(_('Name'), max_length=255)
    birth_date = models.DateField(_('Date of birth'))
    death_date = models.DateField(_('Date of death'))
    death_cause = models.TextField(_('Cause of death'))
    officer_name = models.CharField(_('Officer\'s name'), max_length=255, null=True, blank=True)
    governorate = models.CharField(_('Governorate'), max_length=100)

    created_at = models.DateTimeField(auto_now_add=True)
    last_update = models.DateTimeField(auto_now=True)

    class Meta:
        verbose_name = _("Martyr")
        verbose_name_plural = _("Martyrs")

    def __str__(self):
        return u"Martyr: %s" % self.name
