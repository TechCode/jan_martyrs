from django.contrib import admin
from martyrs.models import Martyr


class MartyrAdmin(admin.ModelAdmin):
    list_display = ('name', 'governorate', 'death_date',)
    search_fields = ('name',)
    list_filter = ('governorate',)


admin.site.register(Martyr, MartyrAdmin)
