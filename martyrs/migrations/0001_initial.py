# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Martyr',
            fields=[
                ('id', models.AutoField(primary_key=True, serialize=False, verbose_name='ID', auto_created=True)),
                ('name', models.CharField(verbose_name='Name', max_length=255)),
                ('birth_date', models.DateField(verbose_name='Date of birth')),
                ('death_date', models.DateField(verbose_name='Date of death')),
                ('death_cause', models.TextField(verbose_name='Cause of death')),
                ('officer_name', models.CharField(blank=True, verbose_name="Officer's name", max_length=255, null=True)),
                ('governorate', models.CharField(verbose_name='Governorate', max_length=100)),
            ],
            options={
                'verbose_name_plural': 'Martyrs',
                'verbose_name': 'Martyr',
            },
        ),
    ]
