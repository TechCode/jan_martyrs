# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime
from django.utils.timezone import utc


class Migration(migrations.Migration):

    dependencies = [
        ('martyrs', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='martyr',
            name='created_at',
            field=models.DateTimeField(default=datetime.datetime(2015, 7, 11, 8, 53, 1, 69243, tzinfo=utc), auto_now_add=True),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='martyr',
            name='last_update',
            field=models.DateTimeField(auto_now=True, default=datetime.datetime(2015, 7, 11, 8, 53, 5, 412067, tzinfo=utc)),
            preserve_default=False,
        ),
    ]
