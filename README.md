# Martyrs

This project is a mission zero from Yaoota. Martyrs is a website that collect information about `#Jan25 Martyrs` in one place.
    The purpose of this test is to evaluate the ability of the Software Engineer to write efficient, well structured , and documented Python/Django code.

## Development Environment setup
- You need to create new virtualenv (project support and tested on both python2.7 and python 3.4) `pyvenv env`
- install requirements `pip install -r requirements.txt`
- you can run tests by `python manage.py test`
- You may need to generate coverage.py report `coverage run ./manage.py test` then generate the html report `coverage html`. You will find report in `html_coverage_report/index.html`.
- You can run project tests by tox by running `tox`.
