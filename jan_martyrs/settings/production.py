from .base import *

DEBUG = False
ALLOWED_HOSTS = ['*']  # FIXME: Should be changed to the production domain(s)

# Database
# https://docs.djangoproject.com/en/1.8/ref/settings/#databases

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': get_env_variable("MARTYRS_DB_NAME"),
        'USER': get_env_variable("MARTYRS_DB_USERNAME"),
        'PASSWORD': get_env_variable("MARTYRS_DB_PASSWORD"),
        'HOST': 'localhost'
    }
}
